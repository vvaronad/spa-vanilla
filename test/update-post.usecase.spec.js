import { PostsRepository } from '../src/repositories/posts.repository';
import { UpdatePostUseCase } from '../src/usecases/update-post.usecase';

// eslint-disable-next-line no-undef
jest.mock('../src/repositories/posts.repository');

describe('Update post use case', () => {
  it('Should update a post', async () => {
    const localPosts = [{
      id: 1,
      title:
        'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
      content: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto'
    },
    {
      id: 2,
      title: 'qui est esse',
      content: 'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla'
    }];
    const post = {
      id: 1,
      title: 'new tittle',
      content: 'new content, lorem ipsum'
    };

    PostsRepository.mockImplementation(() => {
      return {
        updatePost: () => {
          return post;
        }
      };
    });

    const updatedPosts = await UpdatePostUseCase.execute(localPosts, post);
    expect(updatedPosts.length).toBe(localPosts.length);
    expect(updatedPosts[0].title).toBe(post.title);
  });
});
