import { PostsRepository } from '../src/repositories/posts.repository';
import { DeletePostUseCase } from '../src/usecases/delete-post.usecase';
import { POSTS } from './fixtures/posts';

// eslint-disable-next-line no-undef
jest.mock('../src/repositories/posts.repository');

describe('Delete post use case', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it('Should delete a post', async () => {
    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {
          return {};
        }
      };
    });

    const localPosts = POSTS;
    const postID = 1;
    const newPosts = await DeletePostUseCase.execute(localPosts, postID);
    expect(newPosts.length).toBe(localPosts.length - 1);
  });
});
