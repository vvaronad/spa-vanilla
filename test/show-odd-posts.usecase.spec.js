import { OddPosts } from '../src/usecases/show-odd-posts.usecase';
import { POSTS } from './fixtures/posts';

describe('Show odd posts', () => {
  it('should show only odd posts', async () => {
    const results = OddPosts.execute(POSTS);
    expect(results.every(post => post.id % 2 !== 0)).toBe(true);
  });
});
