import { PostsRepository } from '../src/repositories/posts.repository';
import { POSTS } from './fixtures/posts';
import { CreatePost } from '../src/usecases/create-post.usecase';

// eslint-disable-next-line no-undef
jest.mock('../src/repositories/posts.repository');

describe('Create new post use case', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it('should create a post', async () => {
    const newPost = {
      title: 'new tittle',
      content: 'new content, lorem ipsum'
    };

    PostsRepository.mockImplementation(() => {
      return {
        createPost: () => {
          return { newPost };
        }
      };
    });

    const localPosts = POSTS;
    const postsUpdated = await CreatePost.execute(localPosts, newPost);
    expect(postsUpdated.length).toBe(localPosts.length + 1);
    expect(localPosts.filter(post => post.title === newPost.title)).toBeTruthy();
  });
});
