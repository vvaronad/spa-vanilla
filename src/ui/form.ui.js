import { LitElement, html } from 'lit';

export class FormUI extends LitElement {
  constructor() {
    super();
    this.postTitle = '';
    this.postContent = '';
    this.postSelectedID = 0;
  }

  static get properties() {
    return {
      postTitle: { type: String, state: true, attribute: 'post-title' },
      postContent: { type: String, state: true, attribute: 'post-content' },
      postSelectedID: { type: number, state: true, attribute: 'post-selectedid' }
    };
  }

  createRenderRoot() {
    return this;
  }

  static get observedAttributes() {
    return ['post-title', 'post-content', 'post-selectedid'];
  }

  attributeChangedCallback(name, oldValue, newValue) {

    if (name === 'post-title') {
      this.postTitle = newValue;
    }

    if (name === 'post-content') {
      this.postContent = newValue;
    }
    if (name === 'post-selectedid') {
      this.postSelectedID = newValue;
    }

    this.requestUpdate()
  }

  getTitleInput(event) {
    this.postTitle = event.target.value;
  }

  getContentInput(event) {
    this.postContent = event.target.value;
  }

  handleClick(e) {
    e.preventDefault();
    // eslint-disable-next-line no-undef
    const click = new CustomEvent('submit:click', {
      bubbles: true,
      composed: true,
      detail: {
        action: e.target.id,
        post: {
          title: this.postTitle,
          content: this.postContent,
        }
      }
    });

    if (this.postTitle && this.postContent !== "") {
      this.dispatchEvent(click)
    };
  }


  handleCancel(e) {
    e.preventDefault();
    // eslint-disable-next-line no-undef
    this.postTitle = '';
    this.postContent = '';

    const click = new CustomEvent('cancel:click', {
      bubbles: true,
      composed: true,
    });

    this.dispatchEvent(click);
  }

  render() {
    return html` 
        <sections class="form__section">
        <h2 class="form__title">Post Detail</h2>
        <form class="form__form">
          <label class="form__label" for="post-title">Title</label>
          <input class="form__input"  type="text" id="post-title" name="title" .value=${this.postTitle} @input=${this.getTitleInput}/>
          
          <label class="form__label" for="post-content">Body</label>
          <input class="form__input" type="text" id="post-content" name="content" .value=${this.postContent} @input=${this.getContentInput}/>
          
          ${this.postSelectedID ? '' : html`<input class="button" type="submit" id="submitAdd" value="Add" @click=${this.handleClick}/>`}
          ${this.postSelectedID ? html`<input class="button" type="submit" id="submitUpdate" value="Update" @click=${this.handleClick}/>` : ''}
          ${this.postSelectedID ? html`<input class="button" type="submit" id="submitDelete" value="Delete" @click=${this.handleClick}/>` : ''}
      
          <button class="button--secondary" @click=${this.handleCancel}>Cancel</button>
          </form>
        </section>
        `;
  }
}

// eslint-disable-next-line no-undef
customElements.define('form-ui', FormUI);