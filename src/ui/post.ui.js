import { LitElement, html } from 'lit';

export class PostUI extends LitElement {
    static get properties() {
        return {
            post: { type: Object },
        };
    }

    connectedCallback() {
        super.connectedCallback();
    }

    createRenderRoot() {
        return this;
    }

    onClick(e) {
        // eslint-disable-next-line no-undef
        const postDetails = new CustomEvent('post:details', {
            bubbles: true,
            composed: true,
            detail: {
                title: `${this.post.title}`,
                content: `${this.post.content}`,
                id: this.post.id,
            }
        });

        this.dispatchEvent(postDetails);
    }

    render() {
        return html`
            <article  class="post" @click="${this.onClick}">
                <h3 class="post__title" id="title">${this.post?.title}</h3>
                <p class="post__content" id="content">${this.post?.content}</p>
            </article>
        `;
    }
}

// eslint-disable-next-line no-undef
customElements.define('post-ui', PostUI);
