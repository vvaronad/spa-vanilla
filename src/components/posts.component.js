import { html, LitElement } from 'lit';
import '../ui/post.ui';

export class PostComponent extends LitElement {
    constructor() {
        super();
        this.posts = [];
    }

    static get properties() {
        return {
            posts: { type: Array }
        };
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
        <section>
            <h2 class="postList__title">Posts List</h2>
            <ul>
            ${this.posts?.map((post) => {
            return html`
                <li>
                    <post-ui .post=${post}></post-ui>
                </li>
            `;
        })}
            </ul>
        </section>
        `;
    }
}

// eslint-disable-next-line no-undef
customElements.define('posts-component', PostComponent);
