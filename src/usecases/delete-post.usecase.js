import { PostsRepository } from '../repositories/posts.repository';

export class DeletePostUseCase {
  static async execute(posts, postID) {
    const repository = new PostsRepository();
    await repository.deletePost(postID);
    const updatedPosts = posts.filter(post => post.id !== postID);
    return updatedPosts;
  }
}
