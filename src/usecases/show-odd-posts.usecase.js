export class OddPosts {
  static execute (posts) {
    const oddPosts = posts.filter(post => post.id % 2 !== 0);
    return oddPosts;
  }
}
