import { Post } from '../model/post';
import { PostsRepository } from '../repositories/posts.repository';

export class CreatePost {
  static async execute(posts, post) {
    const repository = new PostsRepository();
    const newPost = new Post({
      title: post.title,
      content: post.content,
      id: posts.length,
    });
    repository.createPost(newPost);
    return [newPost, ...posts];
  }
}
