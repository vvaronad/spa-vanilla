import './main.css';
import { LitElement, html } from 'lit';
import '../src/components/posts.component';
import '../src/ui/post.ui';
import '../src/ui/form.ui';

import { AllPostsUseCase } from './usecases/all-posts.usecase';
import { CreatePost } from './usecases/create-post.usecase';
import { UpdatePostUseCase } from './usecases/update-post.usecase';
import { DeletePostUseCase } from './usecases/delete-post.usecase';

class AppElement extends LitElement {
    constructor() {
        super();
        this.selectedPost = {};
        this.postsList = [];
    }

    static get properties() {
        return {
            selectedPost: { type: Object, state: true },
            postsList: { type: Array, state: true }
        };
    }

    createRenderRoot() {
        return this;
    }

    async connectedCallback() {
        super.connectedCallback();

        this.postsList = await AllPostsUseCase.execute();

        this.addEventListener('post:details', e => {
            this.selectedPost = { ...e.detail };
        });

        this.addEventListener('submit:click', async e => {
            if (e.detail.action === 'submitAdd') {
                this.postsList = await CreatePost.execute(this.postsList, e.detail.post);
                this.reset()
            }
            if (e.detail.action === 'submitUpdate') {
                this.postsList = await UpdatePostUseCase.execute(this.postsList, { ...e.detail.post, id: this.selectedPost.id })
                this.reset()
            }
            if (e.detail.action === 'submitDelete') {
                this.postsList = await DeletePostUseCase.execute(this.postsList, this.selectedPost.id)
                this.reset()
            }
        });

        this.addEventListener('cancel:click', () => {
            this.reset()
        })

    }

    reset() {
        this.selectedPost = {};
        this.requestUpdate();
    }

    render() {
        return html`
        <header class="header">
            <h1>Post</h1>
        </header>
        <main class="app__main">
            <posts-component class="postList__section" .posts=${this.postsList}></posts-component>
            <form-ui post-title=${this.selectedPost?.title ?? ''} post-content=${this.selectedPost?.content ?? ''} post-selectedid="${(this.selectedPost?.id) ?? ''}"></form-ui>
        </main>
        `;
    }
}

// eslint-disable-next-line no-undef
customElements.define('app-element', AppElement);
