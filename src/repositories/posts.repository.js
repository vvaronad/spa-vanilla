import axios from 'axios';

export class PostsRepository {
  async getAllPosts() {
    return await (
      await axios.get('https://jsonplaceholder.typicode.com/posts')
    ).data;
  }

  async createPost(post) {
    return await (axios.post('https://jsonplaceholder.typicode.com/posts', {
      title: post.title,
      body: post.content,
      id: post.id,
      userId: 1
    }));
  }

  async updatePost(post) {
    const result = await (axios.put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
      id: post.id,
      title: post.title,
      body: post.content,
      userId: 1
    }));

    return result.data;
  }

  async deletePost(postID) {
    return await (
      axios.delete(`https://jsonplaceholder.typicode.com/posts/${postID}`)
    );
  }
}
